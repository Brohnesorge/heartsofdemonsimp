class Impse_Magic : Weapon replaces Pistol
{
	private action int A_GetNextSpellIndex(int currentspell)
	{
		let plr = TheImp(self);
		if (plr)
		{
			//Wraps around the spells. Gets the last available spell first.
			int index = 0;
			for (int i = index; i < plr.AvailableSpells.Size(); ++i)
				if (plr.AvailableSpells[i]) index = i;

			if (currentspell == index) return 0;

			for (int i = currentspell + 1; i < plr.AvailableSpells.Size(); ++i) //Skips unavailable spells.
			{
				if (!plr.AvailableSpells[i]) continue;
				else return i;
			}
		}

		return -1; //This should never happen.
	}
	
	private action void A_SwitchSpellForward()
	{
		let plr = TheImp(self);

		plr.PrimarySpell = A_GetNextSpellIndex(plr.PrimarySpell);
		A_Print("Switching Primary");
		A_LogFloat(plr.PrimarySpell);
		//A_StartSound("effects/SpellSwitch", 55, CHANF_DEFAULT, 1.0, 1.0, frandom(0.85, 1.15));
	}

	private action void A_SwitchSubSpellForward()
	{
		let plr = TheImp(self);

		plr.SubSpell = A_GetNextSpellIndex(plr.SubSpell);
		A_Print("Switching Sub");
		A_LogFloat(plr.SubSpell);
		//A_StartSound("effects/SpellSwitch", 55, CHANF_DEFAULT, 1.0, 1.0, frandom(0.85, 1.15));
	}
  
	action void A_ThrowImpBall()
	{
		let plr = TheImp(self);
		
		A_AlertMonsters();
		
		switch(plr.PrimarySpell)
		{
			case plr.Fire: A_FireProjectile("Impse_ImpBall"); A_StartSound("FireCast", CHAN_WEAPON); break;
			case plr.Ice: A_FireProjectile("Impse_IceBall"); A_StartSound("IceCast", CHAN_WEAPON); break;
			case plr.Bolt: A_FireProjectile("Impse_BoltBall"); A_StartSound("BoltCast", CHAN_WEAPON); break;
			case plr.Earth: A_FireProjectile("Impse_EarthBall"); A_StartSound("ErthCast", CHAN_WEAPON); break;
			case plr.Air: A_FireProjectile("Impse_AirBall"); A_StartSound("AirCast", CHAN_WEAPON); break;
			case plr.Arcane: A_FireProjectile("Impse_ArcaneBall"); break;
		}
		//break;
	}

	Default
	{
		Weapon.SlotNumber 2;
		
		+WEAPON.NOALERT;
		+WEAPON.AMMO_OPTIONAL;
	}
	
	States
	{
		Ready:
			PHND A 1 A_WeaponReady(WRF_ALLOWRELOAD | WRF_ALLOWZOOM);
			Loop;
			
		Select:
			PHND A 1 A_Raise(12);
			Loop;
			
		Deselect:
			PHND A 1 A_Lower(12);
			Loop;
			
		Reload:
			PHND A 10 A_SwitchSpellForward();
			Goto Ready;
			
		Zoom:
			PHND A 10 A_SwitchSubSpellForward();
			Goto Ready;
			
		Fire:
			THND ABCDE 1 A_WeaponOffset(3, 0, WOF_ADD);
			THND E 2;
			THND F 1 A_WeaponOffset(-9, 0, WOF_ADD);
			TNT1 A 0 A_ThrowImpBall();
			THND GH 1 A_WeaponOffset(-9, 0, WOF_ADD);
			TNT1 A 1 A_WeaponOffset(0, 32);
			TNT1 A 0 A_ReFire("SecondThrow");
			Goto Recover;
			
		SecondThrow:
			THND IJKLM 1 A_WeaponOffset(-3, 0, WOF_ADD);
			THND M 2;
			THND N 1 A_WeaponOffset(9, 0, WOF_ADD);
			TNT1 A 0 A_ThrowImpBall();
			THND OP 1 A_WeaponOffset(9, 0, WOF_ADD);
			TNT1 A 1;
			TnT1 A 0 A_ReFire("Fire");
			Goto Recover;
			
		Recover:
			TNT1 A 0 A_WeaponOffset(0, 132);
			PHND AAAA 1 A_WeaponOffset(0, -25, WOF_ADD);
			Goto Ready;
			
		AltFire:
			MHND ABCDEF 2;
			MHND FFFFFFFF 1 A_WeaponOffset(0 + frandom(-2, 2), 32 + frandom(-2, 2));
			MHND GHIJK 1 A_WeaponOffset(-6, 0, WOF_ADD);
			MHND K 4;
			MHND LMN 1 A_WeaponOffset(24, 0, WOF_ADD);
			TNT1 A 3;
			Goto Recover;
	}
}

class Impse_BaseBall : Actor 
{
	int BaseDamageBonus;
	double MaxDamageBonus;
	
	override void tick()
	{
		let plr = TheImp(target);
		BaseDamageBonus = plr.SpellCounter;
		MaxDamageBonus = plr.MagicLevel;
	
		super.Tick();
	}

	Default 
	{
		Projectile;
		
		Height 6;
		Radius 3;
		
		Scale 1.0;
		Alpha 1.0;
		
		Renderstyle "Add";
		
		+NOEXTREMEDEATH;
	}
}

class Impse_ImpBall : Impse_BaseBall 
{
	Default 
	{
		Speed 35;
		DamageFunction (3 + BaseDamageBonus) * random(4, (8 + MaxDamageBonus));
		
		Scale 0.35;
		
		DamageType "Fire";
	}
	
	States 
	{
		Spawn:
			FORB ABCDE 2;
			Loop;
		Death:
			Stop;
	}
}

class Impse_IceBall : Impse_BaseBall
{
	Default 
	{
		Speed 25;
		DamageFunction random((1+BaseDamageBonus), 48 + (3 * MaxDamageBonus));
		
		Scale 0.5;
		
		DamageType "Ice";
	}
	
	States 
	{
		Spawn:
			IORB A -1;
			Stop;
			
		Death:
			Stop;
	}
}

class Impse_BoltBall : Impse_BaseBall
{
	Default 
	{
		Speed 50;
		DamageFunction 18 + BaseDamageBonus + MaxDamageBonus;
		
		DamageType "Electric";
	}
	
	States 
	{
		Spawn:
			BORB AB 2;
			Loop;
			
		Death:
			BORB CDEFGH 1;
			Stop;
	}
}

class Impse_EarthBall : Impse_BaseBall
{
	Default 
	{
		Speed 20;
		DamageFunction (6 + BaseDamageBonus) * random(4, (8 + MaxDamageBonus));
		
		Scale 0.25;
		Gravity 0.35;
		
		Renderstyle "Normal";
		DamageType "Melee";
		
		-NOGRAVITY;
	}
	
	States 
	{
		Spawn:
			RUBB A -1;
			Loop;
			
		Death:
			Stop;
	}
}

class Impse_AirBall : Impse_BaseBall
{
	Default
	{
		Speed 85;
		DamageFunction (2 + BaseDamageBonus) * random(4, (8 + MaxDamageBonus));
		
		Renderstyle "Translucent";
		DamageType "Melee";
		
		Alpha 0.75;
		Scale 0.15;
		
		+FORCEPAIN;
	}
	
	States 
	{
		Spawn:
			AORB A -1;
			Stop;
			
		Death:
			Stop;
	}
}

class Impse_ArcaneBall : Impse_BaseBall
{
	Default 
	{
		Speed 45;
		DamageFunction 8 * random(4, (8 + MaxDamageBonus));
		
		Renderstyle "Add";
		DamageType "Magic";
		
		Scale 0.5;
	}
	
	States 
	{
		Spawn:
			MORB A -1;
			Stop;
		
		Death:
			MORB B 3;
			Stop;
	}
}

class Impse_IceElement : CustomInventory
{
	Default 
	{
		inventory.PickUpMessage "Ice Element learned";
	}
	
	states
	{
		Spawn:
			TNT1 A 0 NoDelay A_JumpIfInventory("HasIce", 1, "DontSpawn", AAPTR_PLAYER1);
			IGEM A 1;
			Loop;
			
		DontSpawn:
			TNT1 A 0 A_SpawnItemEx("Stimpack");
			Stop;
			
		PickUp:
			TNT1 A 0 
			{
				let plr = TheImp(self);
				
				plr.SpellCounter ++;
				plr.AvailableSpells[plr.Ice] = TRUE;
				A_GiveInventory("HasIce");
			}
			Stop;
	}
}

class Impse_BoltElement : CustomInventory
{

	Default 
	{
		inventory.PickUpMessage "Electric Element learned";
	}
	
	states
	{
		Spawn:
			TNT1 A 0 NoDelay A_JumpIfInventory("HasBolt", 1, "DontSpawn", AAPTR_PLAYER1);
			LGEM A 1;
			Loop;
			
		DontSpawn:
			TNT1 A 0 A_SpawnItemEx("StimPack");
			Stop;
			
		PickUp:
			TNT1 A 0 
			{
				let plr = TheImp(self);
				
				plr.SpellCounter ++;
				plr.AvailableSpells[plr.Bolt] = TRUE;
				A_GiveInventory("HasBolt");
			}
			Stop;
	}
}

class Impse_EarthElement : CustomInventory
{

	Default 
	{
		inventory.PickUpMessage "Earth Element learned";
	}
	
	states
	{
		Spawn:
			TNT1 A 0 NoDelay A_JumpIfInventory("HasEarth", 1, "DontSpawn", AAPTR_PLAYER1);
			EGEM A 1;
			Loop;
			
		DontSpawn:
			TNT1 A 0 A_SpawnItemEx("StimPack");
			Stop;
			
		PickUp:
			TNT1 A 0 
			{
				let plr = TheImp(self);
				
				plr.SpellCounter ++;
				plr.AvailableSpells[plr.Earth] = TRUE;
				A_GiveInventory("HasEarth");
			}
			Stop;
	}
}

class Impse_AirElement : CustomInventory
{

	Default 
	{
		inventory.PickUpMessage "Air Element learned";
	}
	
	states
	{
		Spawn:
			TNT1 A 0 NoDelay A_JumpIfInventory("HasAir", 1, "DontSpawn", AAPTR_PLAYER1);
			WIGM A 1;
			Loop;
			
		DontSpawn:
			TNT1 A 0 A_SpawnItemEx("StimPack");
			Stop;
			
		PickUp:
			TNT1 A 0 
			{
				let plr = TheImp(self);
				
				plr.SpellCounter ++;
				plr.AvailableSpells[plr.Air] = TRUE;
				A_GiveInventory("HasAir");
			}
			Stop;
	}
}

class Impse_ArcaneElement : CustomInventory
{

	Default 
	{
		inventory.PickUpMessage "Arcane Element learned";
	}
	
	states
	{
		Spawn:
			TNT1 A 0 NoDelay A_JumpIfInventory("HasArcane", 1, "DontSpawn", AAPTR_PLAYER1);
			ARCG A 1;
			Loop;
			
		DontSpawn:
			TNT1 A 0 A_SpawnItemEx("StimPack");
			Stop;
			
		PickUp:
			TNT1 A 0 
			{
				let plr = TheImp(self);
				
				plr.SpellCounter ++;
				plr.AvailableSpells[plr.Arcane] = TRUE;
				A_GiveInventory("HasArcane");
			}
			Stop;
	}
}

class GemSpawner : RandomSpawner 
{
	Default 
	{
		DropItem "Impse_IceElement", 255, 1;
		DropItem "Impse_BoltElement", 255, 1;
		DropItem "Impse_EarthElement", 255, 1;
		DropItem "Impse_AirElement", 255, 1;
		DropItem "Impse_ArcaneElement", 255, 1;
	}
}

class ShotgunSpawner : CustomInventory replaces Shotgun 
{
	States 
	{
		Spawn:
			TNT1 A 0 NoDelay 
			{
				if (bDropped)
				{
					A_SpawnItemEx("Impse_M1897");
				}
				else A_SpawnItemEx("GemSpawner");
			}
			Stop;
	}
}

class ChaingunSpawner : CustomInventory replaces Chaingun 
{
	States 
	{
		Spawn:
			TNT1 A 0 NoDelay 
			{
				if (bDropped)
				{
					A_SpawnItemEx("Impse_M240");
				}
				else A_SpawnItemEx("GemSpawner");
			}
			Stop;
	}
}

class SpawnerOverChainsaw 	: GemSpawner replaces Chainsaw {}
class SpawnerOverRL 		: GemSpawner replaces RocketLauncher {}
class SpawnerOverSSG 		: GemSpawner replaces SuperShotgun {}
class SpawnerOverPlasma 	: GemSpawner replaces PlasmaRifle {}
class SpawnerOverBFG 		: GemSpawner replaces BFG9000 {}